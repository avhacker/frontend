FROM python:3.7-alpine

MAINTAINER avhcaker <avhacker@gmail.com>

RUN apk add --no-cache tzdata
RUN ln -fs /usr/share/zoneinfo/Asia/Taipei /etc/localtime
RUN set -x \
    && apk add --no-cache \
      musl \
      openssl \
    && apk add --no-cache --virtual build-dependencies \
      gcc \
      linux-headers \
      musl-dev \
      mariadb-dev \
    && pip --no-cache-dir install \
       flask \
       uwsgi \
       mysqlclient \
    && apk del build-dependencies
RUN set -x \
    && apk add mariadb-dev

